package Data;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class First_Task_Data {
    public static WebDriver driver;
    final public String FIND_THIS = "белорусская железная дорога";  //Data we are looking for
    final public String URL_FOR_CHECK = "https://www.rw.by/";    //It's the URL that we are expecting to be opened in the end

    public First_Task_Data(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(name = "q")
    private WebElement SearchField;
    @FindBy(xpath = "(//input[@name='btnK'])[2]")
    private WebElement SearchButton;
    @FindBy(partialLinkText = "rw.by")
    private WebElement FoundResource;



    public void open_resource(){
        driver.get("https://www.google.com/");
    }


    public void Fill_SearchField(String SearchFor){

        SearchField.sendKeys(SearchFor);
    }

    public void Click_Search_Button(){

        SearchButton.submit();
    }

    public void Select_Found_Page(){

        FoundResource.click();
    }

    public String Check_Current_Page_URL(){
        final String url = driver.getCurrentUrl();
        System.out.println("Expected url is: "+URL_FOR_CHECK);
        System.out.println("Current URL is: "+url);
        return url;
    }
}
