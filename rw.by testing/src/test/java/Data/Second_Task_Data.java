package Data;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Second_Task_Data {
    public static WebDriver driver;
    final public String VALUES_TO_CHECK = "all site\n" + "contacts\n" + "passenger services\n" + "tickets\n" + "freight\n" +
            "corporate\n" + "press center";
    final public String BRAND_NAME = "© 2021 Belarusian Railway";

    public Second_Task_Data(WebDriver driver) {
        Second_Task_Data.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "top-tools")
    private WebElement Top_Menu;
    @FindBy(css = ".index-news-list-wrap")
    private WebElement News;
    @FindBy (className = "copyright")
    private WebElement Company_Name;
    @FindBy (id = "main_menu")
    private WebElement Main_Menu;

    public static void Open_Resource(){
        driver.get("https://rw.by");
    }

    public void Choose_English(){
        Top_Menu.findElement(By.linkText("ENG")).click();
    }

    public Integer News_List(){
        final List<WebElement> LINKS = News.findElements(By.className("news-date-intend"));
        return LINKS.size();
    }

    public String Company_Name(){
        return Company_Name.getText();
    }

    public String Main_Menu(){
        return Main_Menu.getText().toLowerCase();
    }
}
