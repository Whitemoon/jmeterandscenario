package Data;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.Random;

public class Third_Task_Data {
    public static WebDriver driver;
    public String randoms_for_search = "";
    final public String ERROR_MESSAGE = "К сожалению, на ваш поисковый запрос ничего не найдено.";   //Expected error text
    final public String ALPHABET = "abcdefghijklmnopqrstuvwxyz1234567890";       //Symbols the randoms are generated from
    final public int EXPECTED_NUMBER_OF_RESULTS = 15;                        //Expected number of search results



    public Third_Task_Data(WebDriver driver) {
        Third_Task_Data.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "searchinp")
    private WebElement SearchField;

    public void Open_Resource(){
        driver.get("https://rw.by");
    }

    public String Random_Text_Search(){
        Random r = new Random();
        for (int i = 0; i <= 19; i++) {
            randoms_for_search += (ALPHABET.charAt(r.nextInt(ALPHABET.length())));
        }
        final String URL_FOR_CHECK = "https://www.rw.by/search/?s=Y&q="+randoms_for_search;
        SearchField.sendKeys(randoms_for_search);
        SearchField.submit();
        return URL_FOR_CHECK;
    }

    public String Get_Current_Url(){
        String url = driver.getCurrentUrl();
        return url;
    }



}