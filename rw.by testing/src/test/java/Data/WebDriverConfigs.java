package Data;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


public class WebDriverConfigs {
    public WebDriver driver;

    @BeforeTest
    /**Initializing the test dependencies*/
    public void Conditions () {
        /* JFYI the 'chromedriver' version must correspond to your chrome browser version. Otherwise, it won't work */
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @AfterTest
    public void AfterTest(){
        driver.close();
    }
}
