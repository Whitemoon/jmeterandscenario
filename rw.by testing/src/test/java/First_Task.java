import Data.First_Task_Data;
import Data.WebDriverConfigs;
import org.testng.Assert;
import org.testng.annotations.Test;

public class First_Task extends WebDriverConfigs {
    @Test
    public void Opening_Site_From_Google () {
        First_Task_Data Do = new First_Task_Data(driver);
        Do.open_resource();
        Do.Fill_SearchField(Do.FIND_THIS);
        Do.Click_Search_Button();
        Do.Select_Found_Page();
        Assert.assertEquals(Do.Check_Current_Page_URL(), Do.URL_FOR_CHECK);
    }
}