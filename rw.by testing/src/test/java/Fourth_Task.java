import Data.WebDriverConfigs;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class Fourth_Task extends WebDriverConfigs {
    @Test
    /**Verification of news search, link address and news found after a search*/
    public void Work_With_Calendar () {
        driver.get("https://rw.by");
        String main_page_url = "https://pass.rw.by/ru/";
        String city_from = "Брест";     //Name of the departure city (b task item)
        String city_to = "Минск";       //Name of the destination city (b task item)
        WebElement search_from = driver.findElement(By.id("acFrom"));
        WebElement search_to = driver.findElement(By.id("acTo"));
        search_from.sendKeys(city_from);    //(b task item)
        search_to.sendKeys(city_to);        //(b task item)
        WebElement calendar = driver.findElement(By.id("yDate"));
        calendar.click();                   //(c task item)
        SimpleDateFormat sdf = new SimpleDateFormat("dd");  //Fetching the system date
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 5);                  //Addind 5 days to the system date
        String newDate = sdf.format(cal.getTime());
        WebElement sutable_date = driver.findElement(By.id("ui-datepicker-div"));
        WebElement click_date = sutable_date.findElement(By.partialLinkText(newDate));
        click_date.click();
        WebElement find_button = driver.findElement(By.className("std-button"));
        find_button.click();
        List<WebElement> schedule = driver.findElements(By.xpath("//span[@class='train-route']"));
        List<WebElement> departure_times = driver.findElements(By.xpath("//div[@class='sch-table__time train-from-time']"));
        String name_first_train = schedule.get(0).getText();
        //Printing schedule to the IDE console
        for (int i = 0; i < schedule.size(); i++) {
            System.out.println(schedule.get(i).getText()+" "+departure_times.get(i).getText());
            System.out.println("");
        }

        WebElement first_link = schedule.get(0);    //Getting the first link (ii.a task item)
        first_link.click();
        WebElement name_first_train_check = driver.findElement(By.xpath("//div[@class='sch-title__title h2']"));
        Assert.assertTrue(name_first_train_check.getText().contains(name_first_train)); //(ii.b task item)
        List<WebElement> trains = driver.findElements(By.xpath("//tr[@class='selected']"));
        Assert.assertTrue(trains.size() >=1);                                   //(ii.c task item)
        WebElement logo = driver.findElement(By.xpath("//img[@src='/media/img/logo.svg']"));
        logo.click();                                                           //Returning to the main page (ii.b task item)
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url, main_page_url);
    }
}
