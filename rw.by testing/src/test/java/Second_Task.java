import Data.Second_Task_Data;
import Data.WebDriverConfigs;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Second_Task extends WebDriverConfigs {
    @Test
    public void Work_With_Main_Page () {
        Second_Task_Data Do = new Second_Task_Data(driver);
        Do.Open_Resource();
        Do.Choose_English();
        Do.News_List();
        Assert.assertTrue(Do.News_List()>=4);                                     //Number of news verification
        Assert.assertTrue(Do.Company_Name().contains(Do.BRAND_NAME));                    //Brand name verification
        Assert.assertEquals(Do.Main_Menu(), Do.VALUES_TO_CHECK);

    }
}
