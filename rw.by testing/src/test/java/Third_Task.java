import Data.Third_Task_Data;
import Data.Third_Task_Data;
import Data.WebDriverConfigs;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class Third_Task extends WebDriverConfigs {
    @Test
    public void Work_With_Search () {
        Third_Task_Data Do = new Third_Task_Data(driver);
        Do.Open_Resource();
        Do.Random_Text_Search();
        String url_for_check = Do.Random_Text_Search();
        Do.Get_Current_Url();
        System.out.println("Current URL is: "+Do.Get_Current_Url());
        System.out.println("Expected URL is: "+url_for_check);
        System.out.println("");
        Assert.assertEquals(Do.Get_Current_Url(), url_for_check);                    //Verification of search URL
        WebElement error_message_checker = driver.findElement(By.className("notetext"));
        String error_message_text = error_message_checker.getText();
        Assert.assertEquals(Do.ERROR_MESSAGE,error_message_text);      //Verification of error text
        driver.findElement(By.id("searchinp")).clear();
        driver.findElement(By.id("searchinp")).sendKeys("Санкт-Петербург");
        driver.findElement(By.id("searchinp")).submit();
        List<WebElement> displayed_result = driver.findElements(By.className("search-preview"));
        System.out.println("Number of found links is: "+displayed_result.size());
        Assert.assertEquals(Do.EXPECTED_NUMBER_OF_RESULTS, displayed_result.size());   //Verification the number of links
        //Fetching the news into the IDE console
        WebElement all = driver.findElement(By.cssSelector(".search-result:nth-child(3)"));
        List<WebElement> elements = all.findElements(By.className("name"));
        for (int i = 0; i < elements.size(); i++) {
            System.out.println(elements.get(i).getAttribute("href"));
            System.out.println(elements.get(i).getText());
            System.out.println("");
        }
    }
}